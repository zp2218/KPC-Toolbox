%% load trace data with marks

load marked_trace_example.mat;

%% Ignore the labels and use the KPC-Toolbox to fit a MAP (D0,D1)

trace = kpcfit_init(T);
MAP = kpcfit_auto(trace); 
%MAP = kpcfit_auto(trace,'NumStates',4);
%% Split the matrix D1 into D11,D12,...,D1c

disp('**Start Fitting MMAP**');
disp('**Use cross moments to decompose the matrix D1**');
x1 = CM_fmincon(T,C,MAP,1);
x2 = CM_fmincon(T,C,MAP,2);
x3 = CM_fmincon(T,C,MAP,3);
x4 = (x1+x2+x3)/3;
disp('**Use forward moments to decompose the matrix D1**');
x5 = BM_fmincon(T,C,MAP,[1,2,3],x4);
disp('**Use backward moments to decompose the matrix D1**');
x6 = FM_fmincon(T,C,MAP,[1,2,3],x5);
x=(x4+x5+x6)/3;

%% Combine the resulting variables into a matrix D11,D12,...,D1C

[D1c] = split_D1(x,C,MAP);

%% Combine a MMAP (DO,D1,D11,D12,...,D1c)

[MMAP] = combine_mmap(MAP,D1c);

%% Checks whether a MMAP is feasible up to the given tolerance.
[TF] = mmap_isfeasible(MMAP);
if (TF==0)
    MMAP = mmap_normalize(MMAP);
    [TF]= mmap_isfeasible(MMAP);
end

disp('** KPC fitting MMAP algorithm completed ** ');

%% display the multi-class moments of the original trace and fitted MMAP
disp(' ');

% forward moments comparison
disp('                   Forward  Moments Comparison          ');
disp(' Original Trace:                    ');
format long e

for ORDERS=1:3
    disp(mtrace_forward_moments(T,C,ORDERS)); 
end

disp(' Fitted MMAP:'); 
format long e

for ORDERS=1:3
    disp(mmap_forward_moments(MMAP,ORDERS)); 
end

% backward moments comparison
disp('                   Backward  Moments Comparison          ');
disp(' Original Trace:                    ');
format long e

for ORDERS=1:3
    disp(mtrace_backward_moments(T,C,ORDERS)); 
end

disp(' Fitted MMAP:'); 
format long e

for ORDERS=1:3
    disp(mmap_backward_moments(MMAP,ORDERS)); 
end

% cross moments comparison
disp('                   Cross  Moments Comparison          ');
disp(' Original Trace:                    ');
format long e

for ORDER=1
    disp(mtrace_cross_moments(T,C,ORDER)); 
end

disp(' Fitted MMAP:'); 
format long e

for ORDER=1
    disp(mmap_cross_moments(MMAP,ORDER)); 
end

forward_moments_MMAP=mmap_forward_moments(MMAP,[1,2,3]);
forward_moments_trace=mtrace_forward_moments(T,C,[1,2,3]);

backward_moments_MMAP=mmap_backward_moments(MMAP,[1,2,3]);
backward_moments_trace=mtrace_backward_moments(T,C,[1,2,3]);

cross_moments_MMAP=mmap_cross_moments(MMAP,1);
cross_moments_trace=mtrace_cross_moments(T,C,1);
