function [CM_MMAP] = MMAP_cross_moments(x,C,MAP,ORDER)
% compute the cross moments based on the formula, represent the 
% cross moments as variables.
%
% Input: 
% - x is the variables
% - C is the marks vector in the trace
% - MAP{D0,D1} 
% - ORDER: order of the moment to compute
%
% Output:
% - a matrix of the cross moments
%

k=ORDER;
pc=mtrace_pc(C);
pie=map_pie(MAP);

nc=length(unique(C)); % compute the number of class
n=length(MAP{2})^2*nc; % compute the total number of variables
l=length(MAP{2})^2; % compute the step size

m=0;
e=ones(length(MAP{2}),1);

D1c=cell(1,nc);
CM_MMAP=zeros(nc,nc);

% assign variables to the corresponding matrix D11,D12,...D1c
for q=1:l:n
    m=m+1;
    D1c{m}=reshape(x(q:q+l-1),length(MAP{2}),length(MAP{2}));
end

% compute cross moments 
for i=1:nc
    for j=1:nc
         CM_MMAP(i,j)=factorial(k)*(1/(pc(i)*pc(j)))*pie*(-MAP{1})^(-1)*D1c{i}*(inv(-MAP{1}))^(k+1)*D1c{j}*e;  
    end  
                                                                                  
end

end




