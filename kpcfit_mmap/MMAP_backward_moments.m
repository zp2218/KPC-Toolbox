function [BM_MMAP] = MMAP_backward_moments(x,C,MAP,ORDERS)
% compute the backward moments based on the formula, represent the 
% backward moments as variables.
%
% Input: 
% - x is the variables
% - C is the marks vector in the trace
% - MAP{D0,D1} 
% - ORDERS: vector with the orders of the moments to compute
%
% Output:
% - a matrix of the backward moments
%

pc=mtrace_pc(C);
pie=map_pie(MAP);
nc=length(unique(C)); % compute the number of class
n=length(MAP{2})^2*nc; % compute the total number of variables
l=length(MAP{2})^2; % compute the step size

m=0;
e=ones(length(MAP{2}),1);

D1c=cell(1,nc);
BM_MMAP=zeros(length(ORDERS),nc);

% assign variables to the corresponding matrix D11,D12,...D1c
for q=1:l:n
    m=m+1;
    D1c{m}=reshape(x(q:q+l-1),length(MAP{2}),length(MAP{2}));
end

% compute backward moments 
for i=1:length(ORDERS)
    for j=1:nc
        k=ORDERS(i);
        BM_MMAP(i,j)=factorial(k)*(1/pc(j))*pie*(inv(-MAP{1}))^(k+1)*D1c{j}*e;  
    end
    
end

end


