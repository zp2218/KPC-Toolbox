function [MAP,MMAP] = kpcfit_mmap_auto(T,C)
% DESCRIPTION
% Automatic fitting of a marked trace T into a Marked Markovian Arrival  
% Process based on the multi-class moments modelling method.
% 
% Input
% - T: the inter-arrival times
% - C: the class labels
%
% Output
% - MAP : a fitted MAP ignored the labels
% - MMAP: a fitted MMAP

%% Ignore the label and use the KPC-Toolbox to fit a MAP (D0,D1)
trace = kpcfit_init(T);
MAP = kpcfit_auto(trace); 
%MAP = kpcfit_auto(trace,'NumStates',4);
%% Split the matrix D1 into D11,D12,...,D1c
disp('**Start Fitting MMAP**');

disp('**Use cross moments to decompose the matrix D1**');
x1 = CM_fmincon(T,C,MAP,1);
% To improve the iteration speed, we use the previous result as the input to the next function.
disp('**Use forward moments to decompose the matrix D1**');
x2 = BM_fmincon(T,C,MAP,[1,2,3],x1);
disp('**Use backward moments to decompose the matrix D1**');
x3 = FM_fmincon(T,C,MAP,[1,2,3],x2);

% to obtain a unique optimal solution
x = (x1+x2+x3)/3;

disp('** KPC fitting MMAP algorithm completed ** ');
%% Combine the resulting variables into a matrix D11,D12,...,D1C
D1c = split_D1(x,C,MAP);

%% Combine a MMAP (DO,D1,D11,D12,...,D1c)
MMAP = combine_mmap(MAP,D1c);

%% Checks whether a MMAP is feasible up to the given tolerance.
TF = mmap_isfeasible(MMAP);
if (TF==0)
    MMAP = mmap_normalize(MMAP);
    TF = mmap_isfeasible(MMAP);
end

%% display multi-class moments of the original trace and the fitted MMAP
disp(' ');

% forward moments comparison
disp('                  Forward  Moments Comparison          ');
disp(' Original Trace:                    ');
format long e

for ORDERS=1:3
    disp(mtrace_forward_moments(T,C,ORDERS)); 
end

disp(' Fitted MMAP:'); 
format long e

for ORDERS=1:3
    disp(mmap_forward_moments(MMAP,ORDERS)); 
end

% backward moments comparison
disp('                  Backward  Moments Comparison          ');
disp(' Original Trace:                    ');
format long e

for ORDERS=1:3
    disp(mtrace_backward_moments(T,C,ORDERS)); 
end

disp(' Fitted MMAP:'); 
format long e

for ORDERS=1:3
    disp(mmap_backward_moments(MMAP,ORDERS)); 
end

% cross moments comparison
disp('                  Cross  Moments Comparison          ');
disp(' Original Trace:                    ');
format long e

for ORDER=1
    disp(mtrace_cross_moments(T,C,ORDER)); 
end

disp(' Fitted MMAP:'); 
format long e

for ORDER=1
    disp(mmap_cross_moments(MMAP,ORDER)); 
end
end