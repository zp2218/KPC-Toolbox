function [CM_mtrace] = mtrace_cross_moments(T,C,ORDER)
% Compute the k-th order moment of the inter-arrival time between an event
% of class i and an event of class j, for all possible pairs of classes.
%
% Input
%   T: inter-arrival times
%   C: the class labels
%   ORDERS: order of the moment
%
% Output
%   CM_mtrace: the element in (i,j) is the k-th order moment of the inter-arrival
%              time between an event of class i and an event of class j

marks = unique(C);
nc = length(marks);

CM_mtrace = zeros(nc,nc);
count = zeros(nc,nc);

for t = 2:length(T)
    for i = 1:nc
        for j = 1:nc
            if C(t-1) == marks(i) && C(t) == marks(j)
                CM_mtrace(i,j) = CM_mtrace(i,j) + T(t)^ORDER;
                count(i,j) = count(i,j) + 1;
            end
        end
    end
end

CM_mtrace = CM_mtrace ./ count;

end

