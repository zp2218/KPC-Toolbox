function [BM_mtrace] = mtrace_backward_moments(T,C,ORDERS,NORM)
% Compute the backward moments of a marked trace.
%
% Input:
% - T:      the inter-arrival times
% - C:      the class labels
% - ORDERS: vector with the orders of the moments to compute
% - NORM:   0 to return B_{i,c}: M_i = sum B_{i,c}
%           1 (default) to return B_{i,c}: M_i = sum B_{i,c} * p_c
%           where
%             M_i is the class independent moment of order i
%             B_{i,c} is the class-c backward moment of order i
%             p_c is the probability of arrivals of class c
% Output:
% - BM_mtrace: the backward moments as a matrix B_{i,c}

% by default, moments are normalized
if nargin == 3
    NORM = 1;
end

MARKS = unique(C);

nc = length(MARKS);

BM_mtrace = zeros(length(ORDERS),nc);

for j = 1:length(ORDERS)
    k = ORDERS(j);
    for c = 1:nc
        BM_mtrace(j,c) = mean(T.^k .* (C==MARKS(c)));
        if NORM
            BM_mtrace(j,c) = BM_mtrace(j,c) * length(T)/sum(C==MARKS(c));
        end
    end
end
end

