function [MMAP] = combine_mmap(MAP,D1c)
% Combine sub-matrices into a representation of MMAP.
%
% Input:
% - MAP: from kpcfit (D0,D1)
% - D1c: a cell array D1c, which includes D11,D12,...,D1c 
% 
% Output:
% - MMAP: {D0,D1,D11,...,D1c}

nc=length(D1c);
MMAP={MAP{1},MAP{2}};
for i=3:nc+2
     MMAP{i}=D1c{i-2};
end
end

