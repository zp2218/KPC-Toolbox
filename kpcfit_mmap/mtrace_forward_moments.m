function [FM_mtrace] = mtrace_forward_moments(T,C,ORDERS,NORM)
% Compute the forward moments of a marked trace.
%
% Input:
% - T:      the inter-arrival times
% - C:      the class labels
% - ORDERS: vector with the orders of the moments to compute
% - NORM:   0 to return F_{i,c}: M_i = sum F_{i,c}
%           1 (default) to return F_{i,c}: M_i = sum F_{i,c} * p_c
%           where
%             M_i is the class independent moment of order i
%             F_{i,c} is the class-c forward moment of order i
%             p_c is the probability of arrivals of class c
% Output:
% - FM_mtrace: the forward moments as a matrix F_{i,c}

% by default, moments are normalized
if nargin == 3
    NORM = 1;
end

MARKS = unique(C);

nc = length(MARKS);

FM_mtrace = zeros(length(ORDERS),nc);

for i = 1:length(ORDERS)
    k = ORDERS(i);
    for c = 1:nc
        FM_mtrace(i,c) = mean(T(2:end).^k .* (C(1:(end-1)) == MARKS(c)));
        if NORM
            FM_mtrace(i,c) = FM_mtrace(i,c) * length(T)/sum(C(1:(end-1))==MARKS(c));
        end
    end
end
end

