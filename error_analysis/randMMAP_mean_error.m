function [error_forward_mean,error_backward_mean,error_cross_mean]=randMMAP_mean_error(state,class,ORDERS,ORDER)
%% set default values
if (nargin == 2)
   ORDERS=[1,2]; % forward and backward moments
   ORDER=1;      % cross moments
end

%% Generate random MMAP

MMAP=mmap_random(state,class);

%% Split the D1 matrix into D11,D12,...,D13
disp('**Begining**');
disp('**Use forward moments to decompose the matrix D1**');
[x1]=rand_FM_fmincon(MMAP,ORDERS);

disp('**Use backward moments to decompose the matrix D1**');
[x2]=rand_BM_fmincon(MMAP,ORDERS,x1);

disp('**Use cross moments to decompose the matrix D1**');
[x3]=rand_CM_fmincon(MMAP,ORDER,x2);

x=(x1+x2+x3)/3;
%% Combine the resulting variables into a matrix D11,D12,...,D1C

[D1c] = rand_split_D1(x,MMAP);

%% Combine a MMAP (DO,D1,D11,D12,...,D1c)
MAP={MMAP{1},MMAP{2}};
[MMAP_FIT] = combine_mmap(MAP,D1c);

%% Checks whether a MMAP is feasible up to the given tolerance.
[TF] = mmap_isfeasible(MMAP_FIT);
if (TF==0)
    MMAP_FIT = mmap_normalize(MMAP_FIT);
end

disp('**END OF FITTING**');
disp('');
%% error analysis
% compute forward moments of the original MMAP and the fitted MMAP
forward_moments_MMAP_FIT=mmap_forward_moments(MMAP_FIT,1);
forward_moments_MMAP=mmap_forward_moments(MMAP,1);

% compute backward moments of the original MMAP and the fitted MMAP
backward_moments_MMAP_FIT=mmap_backward_moments(MMAP_FIT,1);
backward_moments_MMAP=mmap_backward_moments(MMAP,1);

% compute cross moments of the original MMAP and the fitted MMAP
cross_moments_MMAP_FIT=mmap_cross_moments(MMAP_FIT,1);
cross_moments_MMAP=mmap_cross_moments(MMAP,1);

% compute the forward_error
error_forward_mean=error_forward_moments(forward_moments_MMAP,forward_moments_MMAP_FIT);
%fprintf('error_forward_mean: %.4f,   ',error_forward_mean);

% compute the backward_error
error_backward_mean=error_backward_moments(backward_moments_MMAP,backward_moments_MMAP_FIT);
%fprintf('error_backward_mean: %.4f\n',error_backward_mean);

% compute the cross_error
error_cross_mean=error_cross_moments(cross_moments_MMAP,cross_moments_MMAP_FIT);
%fprintf('error_cross_mean: %.4f\n',error_cross_mean);

%% compute error
function [error_forward_mean]=error_forward_moments(forward_moments_MMAP,forward_moments_MMAP_FIT)
    
error_forward=zeros(size(forward_moments_MMAP,1),size(forward_moments_MMAP,2));

for i=1:size(forward_moments_MMAP,1)
    for j=1:size(forward_moments_MMAP,2)
        error_forward(i,j)=abs((forward_moments_MMAP(i,j)-forward_moments_MMAP_FIT(i,j)))/(forward_moments_MMAP(i,j))*100;
    end
end
error_forward_mean=mean(error_forward(:));
end

function [error_backward_mean]=error_backward_moments(backward_moments_MMAP,back_moments_MMAP_FIT)

error_backward=zeros(size(backward_moments_MMAP,1),size(backward_moments_MMAP,2));

for i=1:size(backward_moments_MMAP,1)
    for j=1:size(backward_moments_MMAP,2)
        error_backward(i,j)=abs((backward_moments_MMAP(i,j)-back_moments_MMAP_FIT(i,j)))/(backward_moments_MMAP(i,j))*100;
    end
end
error_backward_mean=mean(error_backward(:));
end

function [error_cross_mean]=error_cross_moments(cross_moments_MMAP,cross_moments_MMAP_FIT)
    
error_cross=zeros(size(cross_moments_MMAP,1),size(cross_moments_MMAP,2));

for i=1:size(cross_moments_MMAP,1)
    for j=1:size(cross_moments_MMAP,2)
        error_cross(i,j)=abs(cross_moments_MMAP(i,j)-cross_moments_MMAP_FIT(i,j))/cross_moments_MMAP(i,j)*100;
    end
end
error_cross_mean=mean(error_cross(:));
end

end


