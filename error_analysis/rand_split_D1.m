function [D1c] = rand_split_D1(x,MMAP)
% Compute the matrix D11,D12,...,D1c after decomposition
% Input:
% - x: the variables of fmincon function (a column vector)
% - MMAP: a random MMAP
% Output:
% - a cell array D1c, which includes D11,D12,...,D1c


nc=length(MMAP)-2; % c is the number of class, C the label vector of the real trace
n=length(MMAP{2})^2*nc; % n is the number of variables
l=length(MMAP{2})^2;

D1c=cell(1,nc);
m=0;

for i =1:l:n
    m=m+1;
    D1c{m}=reshape(x(i:i+l-1),length(MMAP{2}),length(MMAP{2}));
end

end

