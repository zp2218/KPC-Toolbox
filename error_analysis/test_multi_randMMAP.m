%% Test a random with state(n) class(m) multiple times
%compute the mean error of forward, backward and cross moments

disp('Begin......');
[mean_forward_error,mean_backward_error,mean_cross_error]=mean_iter_error(2,2,100);
disp('Finishing');
