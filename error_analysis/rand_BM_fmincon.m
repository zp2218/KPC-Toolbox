function [x,f]=rand_BM_fmincon(MMAP,ORDERS,x0)
%% objective function
    function f=objfun(x)
        f=norm(rand_MMAP_backward(x,MMAP,ORDERS)-mmap_backward_moments(MMAP,ORDERS));
    end

%% number of variables
nc=length(MMAP)-2; % the number of classes
n =length(MMAP{2})^2*nc; % the total number of variables

%% initial points
%x0=rand(n,1);

%% linear constraints
A=[];
b=[];
Aeq=[];
beq=[];
lb=zeros(1,n);
ub=[];

%% nonlinear constraints
     % assign variables to D1c
     function [D1c] = split_D1(x)
        D1c=cell(1,nc);
        m=0;
        l=length(MMAP{2})^2;
        for i =1:l:n
            m=m+1;
            D1c{m}=reshape(x(i:i+l-1),length(MMAP{2}),length(MMAP{2}));
        end  
    end


    function [c,ceq]=nnlcon(x)
        c = [];
        %ceq=[];
        
        % the sum of D1c is equal to D1
        D1c=split_D1(x);
        
        % compute the sum of D1c
        D1c_sum=zeros(length(MMAP{2}),length(MMAP{2}));
        for i=1:nc
            D1c_sum=D1c_sum+D1c{i};
        end
        
        ceq=D1c_sum-MMAP{2};
            
    end
%% options
options = optimset();
options.Display = 'iter';
options.LargeScale = 'off';
options.MaxIter =  1000;
options.MaxFunEvals = 1e10;
options.MaxSQPIter = 500;
options.TolCon = 1e-8;
options.Algorithm = 'interior-point';
options.OutputFcn =  @outfun;
options.PlotFcn='optimplotx';

MaxCheckIter=1000;
T0 = tic; % needed for outfun


%% optimization program
[x,f]=fmincon(@objfun,x0,A,b,Aeq,beq,lb,ub,@nnlcon,options);

%% outfun
 function stop = outfun(~, optimValues, state)
        global MAXTIME;
        
        stop = false;
        if strcmpi(state,'iter')
            if mod(optimValues.iteration,MaxCheckIter)==0 && optimValues.iteration>1
                reply = input('Do you want more? Y/N [Y]: ', 's');
                if isempty(reply)
                    reply = 'Y';
                end
                
                if strcmpi(reply,'N')
                    stop=true;
                end
            end
            if toc(T0)>MAXTIME
                fprintf('Time limit reached. Aborting.\n');
                stop = true;
            end
        end
 end
end

