function [FOR_RAN_CM] = rand_MMAP_corss(x,MMAP,ORDER)
% compute the cross moments based on the formula, represent the 
% cross moments as variables.
%
% Input: 
% - x is the variables
% - MMAP: a random MMAP 
% - ORDER: order of the moment to compute
%
% Output:
% - a matrix of the cross moments
%
pc=mmap_pc(MMAP);
MAP={MMAP{1},MMAP{2}};
pie=map_pie(MAP);

nc=length(MMAP)-2; % compute the number of class
n=length(MMAP{2})^2*nc; % compute the total number of variables
l=length(MMAP{2})^2; % compute the step size

m=0;
e=ones(length(MMAP{2}),1);

D1c=cell(1,nc);
FOR_RAN_CM=zeros(length(ORDER),nc);

% assign variables to the corresponding matrix D11,D12,...D1c
for q=1:l:n
    m=m+1;
    D1c{m}=reshape(x(q:q+l-1),length(MMAP{2}),length(MMAP{2}));
end

% compute backward moments 
for i=1:length(ORDER)
    for j=1:nc
        k=ORDER(i);
        FOR_RAN_CM(i,j)=factorial(k)*(1/(pc(i)*pc(j)))*pie*(-MMAP{1})^(-1)*D1c{i}*(inv(-MMAP{1}))^(k+1)*D1c{j}*e;  
    end
    
end

end



