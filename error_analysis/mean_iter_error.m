function [forward_error,backward_error,cross_error]=mean_iter_error(state,class,num)
forward_error=zeros(num,1);
backward_error=zeros(num,1);
cross_error=zeros(num,1);

for i=1:num
    fprintf('**Iter %d**  \n',i);
    [error_forward_mean,error_backward_mean,error_cross_mean]=randMMAP_mean_error(state,class);
    
    forward_error(i)=error_forward_mean;
    backward_error(i)=error_backward_mean;
    cross_error(i)=error_cross_mean;
end

end