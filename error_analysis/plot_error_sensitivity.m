%% plot the number of states increase
% forward_mean1=mean(forward_error1);
% forward_mean2=mean(forward_error2);
% forward_mean3=mean(forward_error3);
% forward_mean4=mean(forward_error4);
% forward_mean5=mean(forward_error5);
% forward_std1=std(forward_error1);
% forward_std2=std(forward_error2);
% forward_std3=std(forward_error3);
% forward_std4=std(forward_error4);
% forward_std5=std(forward_error5);
% n=sqrt(20);
% mean_total=[forward_mean1,forward_mean2,forward_mean3,forward_mean4,forward_mean5];
% std_total=[forward_std1/n,forward_std2/n,forward_std3/n,forward_std4/n,forward_std5/n];
% stepsize=[2,4,8,16,32];
% errorbar(stepsize,mean_total,std_total,'-ob');
% title('Test error sensitivity');
% xlabel('The number of states');
% ylabel('The relative error rate (%)');
% legend('forward moments');

% backward_mean1=mean(backward_error1);
% backward_mean2=mean(backward_error2);
% backward_mean3=mean(backward_error3);
% backward_mean4=mean(backward_error4);
% backward_mean5=mean(backward_error5);
% backward_std1=std(backward_error1);
% backward_std2=std(backward_error2);
% backward_std3=std(backward_error3);
% backward_std4=std(backward_error4);
% backward_std5=std(backward_error5);
% n=sqrt(20);
% mean_total=[backward_mean1,backward_mean2,backward_mean3,backward_mean4,backward_mean5];
% std_total=[backward_std1/n,backward_std2/n,backward_std3/n,backward_std4/n,backward_std5/n];
% stepsize=[2,4,8,16,32];
% errorbar(stepsize,mean_total,std_total,'-or');
% title('Test error sensitivity');
% xlabel('The number of states');
% ylabel('The relative error rate (%)');
% legend('backward moments');

% cross_mean1=mean(cross_error1);
% cross_mean2=mean(cross_error2);
% cross_mean3=mean(cross_error3);
% cross_mean4=mean(cross_error4);
% cross_mean5=mean(cross_error5);
% cross_std1=std(cross_error1);
% cross_std2=std(cross_error2);
% cross_std3=std(cross_error3);
% cross_std4=std(cross_error4);
% cross_std5=std(cross_error5);
% n=sqrt(20);
% mean_total=[cross_mean1,cross_mean2,cross_mean3,cross_mean4,cross_mean5];
% std_total=[cross_std1/n,cross_std2/n,cross_std3/n,cross_std4/n,cross_std5/n];
% stepsize=[2,4,8,16,32];
% errorbar(stepsize,mean_total,std_total,'-om');
% title('Test error sensitivity');
% xlabel('The number of states');
% ylabel('The relative error rate (%)');
% legend('cross moments');

%% plot the number of classes increase
% forward_mean1=mean(forward_error1);
% forward_mean2=mean(forward_error2);
% forward_mean3=mean(forward_error3);
% forward_mean4=mean(forward_error4);
% forward_mean5=mean(forward_error5);
% forward_std1=std(forward_error1);
% forward_std2=std(forward_error2);
% forward_std3=std(forward_error3);
% forward_std4=std(forward_error4);
% forward_std5=std(forward_error5);
% n=sqrt(50);
% mean_total=[forward_mean1,forward_mean2,forward_mean3,forward_mean4,forward_mean5];
% std_total=[forward_std1/n,forward_std2/n,forward_std3/n,forward_std4/n,forward_std5/n];
% stepsize=[2,4,6,8,10];
% errorbar(stepsize,mean_total,std_total,'-ob');
% title('Test error sensitivity');
% xlabel('The number of classes');
% ylabel('The relative error rate (%)');
% legend('forward moments');

% backward_mean1=mean(backward_error1);
% backward_mean2=mean(backward_error2);
% backward_mean3=mean(backward_error3);
% backward_mean4=mean(backward_error4);
% backward_mean5=mean(backward_error5);
% backward_std1=std(backward_error1);
% backward_std2=std(backward_error2);
% backward_std3=std(backward_error3);
% backward_std4=std(backward_error4);
% backward_std5=std(backward_error5);
% n=sqrt(50);
% mean_total=[backward_mean1,backward_mean2,backward_mean3,backward_mean4,backward_mean5];
% std_total=[backward_std1/n,backward_std2/n,backward_std3/n,backward_std4/n,backward_std5/n];
% stepsize=[2,4,6,8,10];
% errorbar(stepsize,mean_total,std_total,'-or');
% title('Test error sensitivity');
% xlabel('The number of classes');
% ylabel('The relative error rate (%)');
% legend('backward moments');

cross_mean1=mean(cross_error1);
cross_mean2=mean(cross_error2);
cross_mean3=mean(cross_error3);
cross_mean4=mean(cross_error4);
cross_mean5=mean(cross_error5);
cross_std1=std(cross_error1);
cross_std2=std(cross_error2);
cross_std3=std(cross_error3);
cross_std4=std(cross_error4);
cross_std5=std(cross_error5);
n=sqrt(50);
mean_total=[cross_mean1,cross_mean2,cross_mean3,cross_mean4,cross_mean5];
std_total=[cross_std1/n,cross_std2/n,cross_std3/n,cross_std4/n,cross_std5/n];
stepsize=2:2:10;
errorbar(stepsize,mean_total,std_total,'-om');
title('Test error sensitivity');
xlabel('The number of classes');
ylabel('The relative error rate (%)');
legend('cross moments');