%% increase the number of classes to check the sensitivity
disp('Begin......');

fprintf('Current execution model contains 2 classes\n');
[forward_error1,backward_error1,cross_error1]=mean_iter_error(2,2,50);
fprintf('\n');

fprintf('Current execution model contains 4 classes\n');
[forward_error2,backward_error2,cross_error2]=mean_iter_error(2,4,50);
fprintf('\n');

fprintf('Current execution model contains 6 classes\n');
[forward_error3,backward_error3,cross_error3]=mean_iter_error(2,6,50);
fprintf('\n');

fprintf('Current execution model contains 8 classes\n');
[forward_error4,backward_error4,cross_error4]=mean_iter_error(2,8,50);
fprintf('\n');

fprintf('Current execution model contains 10 classes\n');
[forward_error5,backward_error5,cross_error5]=mean_iter_error(2,10,50);
fprintf('\n');

disp('Finishing');
%% increase the number of states to check the sensitivity
% disp('Begin......');
% 
% fprintf('Current execution model contains 2 states\n');
% [forward_error1,backward_error1,cross_error1]=mean_iter_error(2,2,20);
% fprintf('\n');
% 
% fprintf('Current execution model contains 4 states\n');
% [forward_error2,backward_error2,cross_error2]=mean_iter_error(4,2,20);
% fprintf('\n');
% 
% fprintf('Current execution model contains 8 states\n');
% [forward_error3,backward_error3,cross_error3]=mean_iter_error(8,2,20);
% fprintf('\n');
% 
% fprintf('Current execution model contains 16 states\n');
% [forward_error4,backward_error4,cross_error4]=mean_iter_error(16,2,20);
% fprintf('\n');
% 
% fprintf('Current execution model contains 32 states\n');
% [forward_error5,backward_error5,cross_error5]=mean_iter_error(32,2,20);
% fprintf('\n');
% 
% disp('Finishing');