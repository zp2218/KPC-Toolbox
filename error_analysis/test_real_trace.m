%% load trace data with marks

%load lbl-tcp-3.mat;
load example.mat;

%% Ignore the label and use the KPC-Toolbox to fit a MAP (D0,D1)

trace = kpcfit_init(T);
MAP = kpcfit_auto(trace); 
% MAP = kpcfit_auto(trace,'NumStates',64); 

%% Split the D1 matrix into D11,D12,...,D13
disp('**Use cross moments moments to decompose the matrix D1**');
x1 = CM_fmincon(T,C,MAP,1);

disp('**Use backward moments to decompose the matrix D1**');
x2 = BM_fmincon(T,C,MAP,[1,2],x1);

disp('**Use forward moments to decompose the matrix D1**');
x3 = FM_fmincon(T,C,MAP,[1,2],x2);

x=(x1+x2+x3)/3;
%% Combine the resulting variables into a matrix D11,D12,...,D1C

D1c = split_D1(x,C,MAP);

%% Combine a MMAP (DO,D1,D11,D12,...,D1c)

MMAP = combine_mmap(MAP,D1c);

%% Checks whether a MMAP is feasible up to the given tolerance.
[TF] = mmap_isfeasible(MMAP);
if (TF==0)
    MMAP = mmap_normalize(MMAP);
end

disp('**END OF FITTING**');
%% error analysis
% compute the forward moments of the trace and the fittd MMAP
forward_moments_MMAP=mmap_forward_moments(MMAP,[1,2,3]);
forward_moments_trace=mtrace_forward_moments(T,C,[1,2,3]);

% compute the backward moments of the trace and the fittd MMAP
backward_moments_MMAP=mmap_backward_moments(MMAP,[1,2,3]);
backward_moments_trace=mtrace_backward_moments(T,C,[1,2,3]);

% compute the cross moments of the trace and the fittd MMAP
cross_moments_MMAP=mmap_cross_moments(MMAP,1);
cross_moments_trace=mtrace_cross_moments(T,C,1);

% compute the error
error_forward_mean=error_forward_moments(forward_moments_trace,forward_moments_MMAP);
error_backward_mean=error_backward_moments(backward_moments_trace,backward_moments_MMAP);
error_cross_mean=error_cross_moments(cross_moments_trace,cross_moments_MMAP);

%% compute the mean of error moments
function [error_forward_mean]=error_forward_moments(forward_moments_trace,forward_moments_MMAP)
error_forward=zeros(size(forward_moments_trace,1),size(forward_moments_trace,2));
for i=1:size(forward_moments_trace,1)
    for j=1:size(forward_moments_trace,2)
        error_forward(i,j)=abs((forward_moments_trace(i,j)-forward_moments_MMAP(i,j)))/(forward_moments_trace(i,j))*100;
    end
end
error_forward_mean=mean(error_forward(:));
end

function [error_backward_mean]=error_backward_moments(backward_moments_trace,backward_moments_MMAP)
error_backward=zeros(size(backward_moments_trace,1),size(backward_moments_trace,2));
for i=1:size(backward_moments_trace,1)
    for j=1:size(backward_moments_trace,2)
        error_backward(i,j)=abs((backward_moments_trace(i,j)-backward_moments_MMAP(i,j)))/(backward_moments_trace(i,j))*100;
    end
end
error_backward_mean=mean(error_backward(:));
end


function [error_cross_mean]=error_cross_moments(cross_moments_trace,cross_moments_MMAP)
error_cross=zeros(size(cross_moments_trace,1),size(cross_moments_trace,2));
for i=1:size(cross_moments_trace,1)
    for j=1:size(cross_moments_trace,2)
        error_cross(i,j)=abs((cross_moments_trace(i,j)-cross_moments_MMAP(i,j)))/(cross_moments_trace(i,j))*100;
    end
end
error_cross_mean=mean(error_cross(:));
end