%% Test random traces
%compute the mean error of forward, backward and cross moments.
disp('Begin......');
num=10;
error_forward_total=zeros(num,1);
error_backward_total=zeros(num,1);
error_cross_total=zeros(num,1);
for i=1:num
    fprintf('*iter %d*\n',i);
    [error_forward_mean,error_backward_mean,error_cross_mean]=randSample_error(8,10e4);
    error_forward_total(i)=error_forward_mean;
    error_backward_total(i)=error_backward_mean;
    error_cross_total(i)=error_cross_mean;
end
disp('Finishing');