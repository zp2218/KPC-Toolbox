function [FOR_RAN_BM] = rand_MMAP_backward(x,MMAP,ORDERS)
% compute the backward moments based on the formula, represent the 
% backward moments as variables.
%
% Input: 
% - x is the variables
% - MMAP: a random MMAP
% - ORDERS: vector with the orders of the moments to compute
%
% Output:
% - a matrix of the backward moments

pc=mmap_pc(MMAP);
MAP={MMAP{1},MMAP{2}};
pie=map_pie(MAP);

nc=length(MMAP)-2; % compute the number of class
n=length(MMAP{2})^2*nc; % compute the total number of variables
l=length(MMAP{2})^2; % compute the step size

m=0;
e=ones(length(MMAP{2}),1);

D1c=cell(1,nc);
FOR_RAN_BM=zeros(length(ORDERS),nc);

% variables=zeros(n,1);

% for i=1:n
%     variables(i)=x(i);
% end

% assign variables to the corresponding matrix D11,D12,...D1c
for q=1:l:n
    m=m+1;
    D1c{m}=reshape(x(q:q+l-1),length(MMAP{2}),length(MMAP{2}));
end

% compute backward moments 
for i=1:length(ORDERS)
    for j=1:nc
        k=ORDERS(i);
        FOR_RAN_BM(i,j)=factorial(k)*(1/pc(j))*pie*(inv(-MMAP{1}))^(k+1)*D1c{j}*e;  
    end
    
end

end

