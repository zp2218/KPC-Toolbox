function [D1c] = test_mmap_D1c(x,C,MAP)

% k=ORDERS;
% pc=mtrace_pc(C);
% pie=map_pie(MAP);
% e=ones(length(MAP{2}),1);
nc=length(unique(C)); % compute the number of class
n=length(MAP{2})^2*nc; % compute the total number of variables
l=length(MAP{2})^2; % compute the step size
m=0;
%BM_formula=zeros(1,nc);
% FM_formula=zeros(1,nc);

% variables=zeros(n,1);

% for i=1:n
%     variables(i)=x(i);
% end

D1c=cell(1,nc);

for i=1:l:n
    m=m+1;
    D1c{m}=reshape(x(i:i+l-1),length(MAP{2}),length(MAP{2}));
end

% for j=1:nc
%     %BM_formula(1,j)=factorial(k)*(1/pc(j,1))*pie*(-inv(MAP{1}))^(k+1)*D1c{j}*e;
%     FM_formula(1,j)=factorial(k)*(1/pc(j,1))*pie*(-MAP{1})^(-1)*D1c{j}*(-MAP{1})^(-k)*e;
%     
% end

end




