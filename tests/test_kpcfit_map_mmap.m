disp('**Please select the statistic descriptor used to decompose the matrix D1**');
disp('1:Cross moments     2:Forward moments     3:Backward moments');

method = input('Enter a number: ');

switch method
    case 1
        disp('*using cross moments to split the D1');
        ORDERS=input('please enter the order of the moment (a number): ');
        [x,f]=test_CM_fmincon(S,C,MAP,ORDERS);
    case 2
        disp('using forward moments to split the D1');
        ORDERS=input('please enter the order of the moment (a number or a vector): ');
        [x,f]=test_FM_fmincon(S,C,MAP,ORDERS);
    otherwise
        disp('using backward moments to split the D1');
        ORDERS=input('please enter the order of the moment (a number or a vector): ');
        [x,f]=test_BM_fmincon(S,C,MAP,ORDERS);
end