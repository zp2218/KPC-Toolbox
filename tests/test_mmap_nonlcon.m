function [c,ceq]=test_mmap_nonlcon(x,C,MAP)
c = [];
nc=length(unique(C));
% the sum of D1c is equal to D1
D1c=D1_split(x,C,MAP);
D1c_sum=zeros(length(MAP{2}),length(MAP{2}));
for i=1:nc
    D1c_sum=D1c_sum+D1c{i};
end
ceq = D1c_sum-MAP{2};                   
end

