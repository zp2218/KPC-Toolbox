function [f]=test_mmap_objfun(x,S,C,MAP,ORDERS)
%f=norm(formula_forward_moments(x,C,MAP,ORDERS)-mtrace_forward_moments(S,C,ORDERS));
%f=norm(mtrace_backward_moments(S,C,ORDERS)-formula_backward_moments(x,C,MAP,ORDERS));
f=norm(formula_cross_moments(x,C,MAP,ORDERS)-mtrace_cross_moments(S,C,ORDERS));
end




