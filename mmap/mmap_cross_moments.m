function CM_mmap = mmap_cross_moments(MMAP,ORDER)
% Computes the k-th order moment of the inter-arrival time between an event
% of class i and an event of class j, for all possible pairs of classes.
%
% Input:
% - MMAP:   The MMAP
% - ORDER:  The order of the moments to be computed.
%
% Output
% - CM_mmap: The element in (i,j) is the k-th order moment of the inter-arrival
%            time between an event of class i and an event of class j


c = length(MMAP)-2;  %calculate the number of classes
D0 = MMAP{1};
pie = map_pie(MMAP);
CM_mmap=zeros(c,c);
e=ones(size(MMAP{1},1),1);

for i=1:c
    for j=1:c
        pcn=pie*(-MMAP{1}\MMAP{2+i})*(-MMAP{1}\MMAP{2+j})*e;
        CM_mmap(i,j)=factorial(ORDER)*(1/pcn)*pie*(-MMAP{1}\MMAP{i+2})*(inv(-D0)^ORDER)*(-MMAP{1}\MMAP{j+2})*e;
    end
end

end

