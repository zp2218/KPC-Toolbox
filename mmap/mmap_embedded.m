function Pc = mmap_embedded(MMAP)
m = length(MMAP) - 2;
Pc = cell(1,m);
for i=1:m
    Pc{i} = -MMAP{1} \ MMAP{2+i};
end
end
