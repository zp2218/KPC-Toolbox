function FM_mmap = mmap_forward_moments(MMAP,ORDERS)
% Computes the theoretical forward moments of an MMAP. 
%
% Input:
% - MMAP:   The MMAP
% - ORDERS: A vector of the order of the moments to be computed.
%
% Output:
% - FM_mmap: The forward moments as a matrix F_{k,c}, k denotes the k-th
%            order, c denotes the arrival class

c=length(MMAP)-2; %calculate the number of classes
k=length(ORDERS);
D0 = MMAP{1};
pie = map_pie(MMAP);
pc=mmap_pc(MMAP);
FM_mmap=zeros(k,c);
e=ones(size(MMAP{1},1),1);

% i control the orders
% j control the number of classes
for i = 1:length(ORDERS)
    k=ORDERS(i);
    for j = 1:c
        FM_mmap(i,j)=factorial(k)*(1/pc(j,1))*pie*((-MMAP{1})\(MMAP{2+j})*(inv(-D0)^k))*e;
    end
        
end

end

