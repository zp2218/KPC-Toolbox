function pie = mmap_pie(MMAP)
% For each class c, computes the stationary probability of the DTMC
% embedded at the restart intants after an arrival of class c.
%
% Input:
% - MMAP: the MMAP of order n with m classes
%
% Output:
% - pie:  an mxn matrix, where the c-th row is the solution for class c

% n denotes the number of states 
n = size(MMAP{1},1);

% m denotes the number of classes
m = size(MMAP,2)-2;

pie=zeros(m,n);

for c = 1:m
    Pc = (-MMAP{1}-MMAP{2}+MMAP{2+c})\MMAP{2+c};
    A = Pc'-eye(n);
    A(end,:) = ones(1,n);    
    b = zeros(n,1);    
    b(n) = 1;     
    pie(c,:) = (A\b)';
end

end

