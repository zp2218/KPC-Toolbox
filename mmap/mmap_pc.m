function pc = mmap_pc(MMAP)
% Computes the arrival probabilities of each class for the given MMAP.
%
% Input:
% - MMAP: The MMAP
%
% Output:
% - pc: The arrival probabilities of each class as a column vector

c = length(MMAP)-2;
pc=zeros(c,1);
e=ones(size(MMAP{1},1),1);
for i = 1:c
    pc(i)=map_pie(MMAP)*((-MMAP{1})\(MMAP{2+i}))*e;
end
end

