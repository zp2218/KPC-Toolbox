function BM_mmap = mmap_backward_moments(MMAP,ORDERS)
% Computes the theoretical backward moments of an MMAP. 
%
% Input:
% - MMAP:   The MMAP
% - ORDERS: A vector of the order of the moments to be computed.
%
% Output:
% - BM_mmap: The backard moments as a matrix B_{k,c}, k denotes the k-th
%            order, c denotes the arrival class

c=length(MMAP)-2; %calculate the number of classes
k=length(ORDERS);
D0 = MMAP{1};
pie = map_pie(MMAP);
pc=mmap_pc(MMAP);
BM_mmap=zeros(k,c);
e=ones(size(MMAP{1},1),1);

for i=1:length(ORDERS)
    k=ORDERS(i);
    for j = 1:c
        BM_mmap(i,j)=factorial(k)*(1/pc(j,1))*pie*(inv(-D0)^k)*((-MMAP{1})\(MMAP{2+j}))*e;
    end
end

end

