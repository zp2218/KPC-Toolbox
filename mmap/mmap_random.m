function MMAP=mmap_random(ORDER,CLASSES)
% Randomly generate an MMAP that specify the number of states and classes
%
% Input:
% - ORDER: Specity the number of states of the MMAP
% - CLASSES: Specify the number of classes of the MMAP
%
% Output:
% - MMAP: A random MMAP

MAP = map_rand(ORDER);
MMAP{1} = MAP{1};
MMAP{2} = MAP{2};

for i=1:ORDER
    p = rand(1,CLASSES);
    p = p / sum(p);
    for j=1:ORDER
        for c=1:CLASSES
            MMAP{2+c} = MMAP{2}*p(c);
        end
    end
end
end

